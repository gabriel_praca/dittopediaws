const express = require('express');
const app = express();

const mySql = require('mysql');
const conn = mySql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'dittopedia_dev'
});

const port = 3001;

app.get("/", (req, res) => {
    console.log("root route");
    res.send("Hello from root");
});

app.get("/Pokemons", (req, res) => {
    console.log("Get Pokemons");
    let test;
    conn.query("select * from pokemon", (err, rows, fields) => {
        if(err){
            console.log(err);
            console.log(req);
        }
        else{
            console.log(rows);
            res.send(rows);
        }
    });
});

app.get("/Pokemon/:id/types", (req, res) => {
    let pokemon_id = req.params.id;
    let queryString = "select * from pokemon_type pt inner join types_of_pokemon top on top.pokemon_type_id = pt.pokemon_type_id and top.pokemon_id = ?";
    
    conn.query(queryString, [pokemon_id], (err, rows, fields) => {
        if(err){
            console.log(err);
            console.log(req);
        }
        else{
            console.log(rows);
            res.send(rows);
        }
    });
});

app.listen(port, () => {
    console.log("Server is up on 3001 port!")
});